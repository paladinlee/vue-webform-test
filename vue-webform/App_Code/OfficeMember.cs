﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

/// <summary>
/// OfficeMember 的摘要描述
/// </summary>
public class OfficeMember
{
    public OfficeMember()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }

    public string GetAllMember()
    {
        //模擬一個 DataTable --Start
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MemberName", System.Type.GetType("System.String")));
        dt.Columns.Add(new DataColumn("Gender", System.Type.GetType("System.String")));

        DataRow dr = null;        

        //Add record1
        dr = dt.NewRow();
        dr["MemberName"] = "paladin";
        dr["Gender"] = "男";
        dt.Rows.Add(dr);

        //Add record2
        dr = dt.NewRow();
        dr["MemberName"] = "ruby";
        dr["Gender"] = "女";
        dt.Rows.Add(dr);

        return JsonConvert.SerializeObject(dt);
    }
}