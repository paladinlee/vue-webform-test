﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="./css/Default.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div v-cloak id="app">
            {{message}}
            <div><button type="button" @click="GetMember">Get</button></div>
            <div>member list:</div>
            <div>
                <ol>
                    <li v-for="v in members">{{v.MemberName}}({{v.Gender}})</li>
                </ol>
            </div>
        </div>
    </form>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="./js/Default.js"></script>
</body>
</html>
